/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Fichero: Ejemplo0504.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 20-ene-2014
 */
public class Ejemplo0504 {

  public static void main(String args[]) {
    int v1[] = {3, 4, 5};
    int v2[] = {3, 4, 5};
    int v3[] = v1.clone();
    int v4[] = v1;
    if (v1 != v2) {
      System.out.println("v1 != v2");
    }
    if (v1 != v3) {
      System.out.println("v1 != v3");
    }
    if (v1 == v4) {
      System.out.println("v1 == v4");
    }
    if (!v1.equals(v2)) {
      System.out.println("!v1.equals(v2)");
    }
    if (!v1.equals(v3)) {
      System.out.println("!v1.equals(v3)");
    }
    if (v1.equals(v4)) {
      System.out.println("v1.equals(v4)");
    }
  }
}
/* EJECUCION:
 v1 != v2
 v1 != v3
 v1 == v4
 !v1.equals(v2)
 !v1.equals(v3)
 v1.equals(v4)
 */
