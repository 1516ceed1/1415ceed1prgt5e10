/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Fichero: Ejemplo0532.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 20-ene-2014
 */
public class Ejemplo0532a {

  public static void main(String args[]) {

    StringBuffer holamundoBuffer = new StringBuffer();
    holamundoBuffer.append("Hola, ");
    holamundoBuffer.append("mundo");

    String holamundo = holamundoBuffer.toString();

    System.out.println(holamundo);

  }
}

/* EJECUCION:
 Hola, mundo
 */
