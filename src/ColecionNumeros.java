
import java.util.ArrayList;
import java.util.Iterator;

public class ColecionNumeros {

    public static void main(String[] args) {
        ArrayList<Integer> lista = new ArrayList();
        //ArrayList<int> lista = new ArrayList(); //  Error
        lista.add(1);
        lista.add(2);
        lista.add(3);

        System.out.println("Numeros Introducidos: ");
        Iterator it = lista.iterator(); // Esta intrucción no se debe poner antes.
        while (it.hasNext()) {
            System.out.println("Numero: " + it.next());
        }
    }
}
/* Ejecucion
Numeros Introducidos:
Numero: 1
Numero: 2
Numero: 3
*/
