/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Fichero: Ejemplo0540HashTable.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 10-ene-2014
 */
public class Ejemplo0540HashTable {

    public static void main(String args[]) {

        Hashtable<String, String> paises = new Hashtable<String, String>();
        paises.put("ES", "Espanya");
        paises.put("UK", "Reino Unido");
        paises.put("US", "Estados Unidos");
        paises.put("FR", "Francia");

        System.out.println("1. Recorrido de las claves");
        Enumeration<String> e = paises.keys();
        while (e.hasMoreElements()) {
            System.out.println(e.nextElement());
        }

        System.out.println("2. Recorrido del hasmap");
        java.util.Enumeration claves = paises.keys();
        while( claves.hasMoreElements() )
        {
        Object clave = claves.nextElement();
        Object valor = paises.get(clave);
        System.out.println("Pais: "+clave.toString()+", capital:  " 
                +valor.toString());
        }
        
        System.out.println("3. Recorrido del hasmap ordenado");
        String[] arrayclaves = (String[]) paises.keySet().toArray(new String[0]);
        java.util.Arrays.sort(arrayclaves); // Ordena el vector
        for (String clave : arrayclaves) {
            System.out.println(clave + " : " + paises.get(clave));
        }

    }
}
/* EJECUCION:
1. Recorrido de las claves
FR
UK
US
ES
2. Recorrido del hasmap
Pais: FR, capital:  Francia
Pais: UK, capital:  Reino Unido
Pais: US, capital:  Estados Unidos
Pais: ES, capital:  Espanya
3. Recorrido del hasmap ordenado
ES : Espanya
FR : Francia
UK : Reino Unido
US : Estados Unidos
 * */
