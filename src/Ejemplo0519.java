/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.Scanner;

/**
 * Fichero: Ejemplo0519.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 20-ene-2014
 */
public class Ejemplo0519 {

  public static void main(String argv[]) {
    String linea;
    Scanner stdir = new Scanner(System.in);

    do {
      System.out.print("Escriba una palabra:");
      linea = stdir.nextLine();
      System.out.println(linea);
    } while (!linea.equals("FIN"));

  }
}
/* EJECUCION:
Escriba una palabra:una
una
Escriba una palabra:dos
dos
Escriba una palabra:FIN
FIN
*/
