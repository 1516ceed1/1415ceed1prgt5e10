/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Fichero: Ejemplo0506.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 20-ene-2014
 */
public class Ejemplo0506 {

  public static void main(String[] args) {
    String[] lista1 = {"ab", "cd", "ef"};
    int[] lista2 = {1, 2, 3};
    for (String s : lista1) {
      System.out.println(s);
    }
    for (int i : lista2) {
      System.out.println(i);
    }
  }
}
/* EJECUCION:
 ab
 cd
 ef
 1
 2
 3
 */
