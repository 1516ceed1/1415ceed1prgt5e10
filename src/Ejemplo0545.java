/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Fichero: Ejemplo0545.java
 * @date 27-ene-2014
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Ejemplo0545 {

public static void main(String[] args) throws ParseException {
    Pattern p = Pattern.compile("\\d\\d\\d");
    Matcher m = p.matcher("a123b");
    System.out.println(m.find());
    System.out.println(m.matches());

    p = Pattern.compile("^\\d\\d\\d$");
    m = p.matcher("123");
    System.out.println(m.find());
    System.out.println(m.matches());
}
}

/* output:
true
false
true
true
*/
