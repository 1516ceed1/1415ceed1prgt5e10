/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Fichero: Ejemplo0507.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 20-ene-2014
 */
public class Ejemplo0507 {

  public static void main(String args[]) {

    final int TALLA = 5;
    int vector[];
    vector = new int[TALLA];
    int suma = 0, media = 0, numero = 0;
    InputStreamReader input = new InputStreamReader(System.in);
    BufferedReader buffer = new BufferedReader(input);
    String linea;


    for (int i = 0; i < TALLA; i++) {
      try {
        System.out.print("Dato: ");

        linea = buffer.readLine();
        numero = Integer.parseInt(linea);

      } catch (Exception e) {
        System.out.println("Error");
      }
      vector[i] = numero;
    }

    for (int i = 0; i < TALLA; i++) {
      suma = suma + vector[i];
    }
    media = suma / TALLA;
    System.out.println("La media es: " + media);
  }
}
/* EJECUCION:
 Dato: 2
 Dato: 32
 Dato: 2
 Dato: 3
 Dato: 2
 La media es: 8
 */
