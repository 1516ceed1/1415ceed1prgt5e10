/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Fichero: Ejemplo0546.java
 * @date 27-ene-2014
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Ejemplo0546 {

    public static boolean validar (String s){
        Pattern p = Pattern.compile("[\\w]+@[\\w]+\\.\\w+");
        Matcher m = p.matcher(s);
        return m.matches();
    }

public static void main(String[] args) throws Exception {
    
      String[] emails = { "test@example.com",
                "test-101@example.com", "test.101@yahoo.com",
                "test101@example.com", "test_101@example.com",
                "test-101@test.net", "test.100@example.com.au", "test@e.com",
                "test@1.com", "test@example.com.com", "test101@example.com",
                "101@example.com", "test-101@example-test.com" };
 
        for (String email : emails) {     
            System.out.println(email + " valido: " + validar(email) );
        }
 
    }
}
 
