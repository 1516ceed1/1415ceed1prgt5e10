
/**
 * Fichero: Ejemplo0501.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 17-ene-2014
 */
public class Ejemplo0501 {
     
    
  public static void main(String args[]) {
    final int TALLA = 5;
    int v[];
    int i, s = 0;
    v = new int[TALLA];
    
    for (i = 0; i < TALLA; i++) {
      v[i] = i;
    }
    for (i = 0; i < TALLA; i++) {
      System.out.println(v[i]);
    };
    for (i = 0; i < v.length ; i++) {
      s = s + v[i];
    };
    System.out.println("La suma es: " + s);
  }
}
/* EJECUCION:
 0
 1
 2
 3
 4
 La suma es: 10
 */
