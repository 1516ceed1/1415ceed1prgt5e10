/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Fichero: Ejemplo0511.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 20-ene-2014
 */
public class Ejemplo0511 {

  public static void main(String args[]) {

    char[] cadena1 = {'a', 'b', 'c'};
    char[] cadena2 = {97, 98, 99}; // ascii de 97 es la 'a'
    char[] cadena3;
    //char [] cadena4="ABC"; // Error. No es correcto.

    System.out.println(cadena1);
    System.out.println(cadena1.length);

    int a = 'a';
    System.out.println(a);// ascii de 97 es la 'a'
    System.out.println(cadena2); // imprime ascii
  }
}

/* EJECUCION:
 abc
 3
 97
 abc*/
