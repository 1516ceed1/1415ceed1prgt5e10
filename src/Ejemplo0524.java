/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Fichero: Ejemplo0524.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 20-ene-2014
 */
public class Ejemplo0524 {

  public void convertStringToCharArray() {
    String str = "Abc";
    char[] cArray = str.toCharArray();
    for (char c : cArray) {
      System.out.println(c);
    }
  }

  public static void main(String[] args) {
    new Ejemplo0524().convertStringToCharArray();
  }
}
/* EJECUCION:
A
b
c
*/
