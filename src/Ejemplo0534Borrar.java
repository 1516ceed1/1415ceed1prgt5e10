/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.ArrayList;

/**
 * Fichero: Ejemplo0534Borrar.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 04-feb-2014
 */
class Ejemplo0534Borrar {

  public static void mostrar(ArrayList<Persona0734> arrayList) {

    System.out.print("LISTADO:");
    // Recorremos la lista.
    int i = 0;
    for (Persona0734 p : arrayList) {
      System.out.print(i + " [" + p.getNombre() + "] ");
      i++;
    }
    System.out.println("");
  }

  public static void main(String[] argc) {

    Persona0734 persona1 = new Persona0734("Julian", 20);
    Persona0734 persona2 = new Persona0734("Betty", 17);
    Persona0734 persona3 = new Persona0734("Marta", 22);
    ArrayList<Persona0734> arrayList = new ArrayList<>();

    arrayList.add(persona1);
    arrayList.add(persona2);
    arrayList.add(persona3);

    mostrar(arrayList);

    arrayList.remove(1);

    mostrar(arrayList);


  }
}

class Persona0734 {

  String nombre;
  int edad;

  Persona0734(String n, int e) {
    nombre = n;
    edad = e;
  }

  String getNombre() {
    return nombre;
  }
}
/* EJECUCIÓN:
 LISTADO:0 [Julian] 1 [Betty] 2 [Marta] 
 LISTADO:0 [Julian] 1 [Marta] 
 */
