
/**
 * Fichero: Ejemplo0532.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 20-ene-2014
 */
public class Ejemplo0532b {

  public static void main(String args[]) {
    StringBuilder holamundoBuilder = new StringBuilder();
    holamundoBuilder.append("Hola, ");
    holamundoBuilder.append("mundo");
    String holamundo = holamundoBuilder.toString();
    System.out.println(holamundo);
  }
}

/* EJECUCION:
 Hola, mundo
 */
