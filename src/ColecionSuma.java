
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;

/*
 Hacer un programa en java que lee numeros por teclado y los almacene en
 un ArrayList y en un HashList.
 */
public class ColecionSuma {

    public static void main(String[] args) {

        //ArrayList<Integer> lista = new ArrayList(); // Cambiar por hashset
        HashSet<Integer> lista = new HashSet();

        Scanner sc = new Scanner(System.in);
        int numero = 1;
        int suma = 0;

        while (numero != 0) {
            System.out.print("Numero: ");
            numero = sc.nextInt();
            lista.add(numero);
            suma = suma + numero;
        }

        System.out.println("Numeros Introducidos: ");
        Iterator it = lista.iterator(); // Esta intrucción no se debe poner antes.
        while (it.hasNext()) {
            System.out.println("Numero: " + it.next());
        }
        System.out.println("La suma es: " + suma);

    }

}
