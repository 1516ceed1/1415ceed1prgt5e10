/**
 * Fichero: Ejemplo0518.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 20-ene-2014
 */
public class Ejemplo0518a {

  public static void main(String args[]) {

    String str1 = "ab";
    String str2 = "ab";

    // igual contenido. true
    System.out.println("str1.equals(str2): " + str1.equals(str2));

    // Mismo obheto. false
    System.out.println("str1 == str2: " + (str1 == str2));

  }
}
/* EJECUCION:
 str1.equals(str2): true
 str1 == str2: true
 */
