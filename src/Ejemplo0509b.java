/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Fichero: Ejemplo0509.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 20-ene-2014
 */
public class Ejemplo0509b {

  public static void Mifuncion1(int par1) {
    par1 = par1 + 1;
  }

  public static void Mifuncion1(int par2[]) {
    par2[0] = par2[0] + 1;
  }

  public static void Mifuncion1(String par3) {
    par3 = par3 + 1;
  }

  public static void Mifuncion1(Integer par4) {
    par4 = par4 + 1;
  }

  public static void main(String args[]) {

    int var1 = 1;
    Mifuncion1(var1);
    System.out.println("var1 " + var1);

    int[] var2 = {2};
    Mifuncion1(var2);
    System.out.println("var2 " + var2[0]);

    String var3 = "String";
    Mifuncion1(var3);
    System.out.println("var3 " + var3);

    Integer var4 = new Integer(4);
    Mifuncion1(var4);
    System.out.println("var4 " + var4);

  }

}
/* EJECUCION:
 var1 1
 var2 3
 var3 String
 var4 4
 */
