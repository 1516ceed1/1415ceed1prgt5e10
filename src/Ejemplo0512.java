/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Fichero: Ejemplo0512.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 20-ene-2014
 */
public class Ejemplo0512 {

  public static void main(String args[]) {

    char[] cadena3;

    // Cadena3. Tiene capacidad para 4 elementos
    cadena3 = new char[4];
    cadena3[0] = 'a';
    cadena3[1] = 'b';
    cadena3[2] = '\0';
    System.out.println(cadena3.length);
    System.out.println(cadena3);

    // Error. length no es un metodo sino una variable.
    // System.out.println(cadena3.length());  // Error en length
  }
}
/* EJECUCION:
4
ab
*/
